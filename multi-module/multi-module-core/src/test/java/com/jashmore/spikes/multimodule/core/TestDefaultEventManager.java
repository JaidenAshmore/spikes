package com.jashmore.spikes.multimodule.core;

import com.jashmore.multimodule.api.Event;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

public class TestDefaultEventManager {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private InternalDependency internalDependency;


    private DefaultEventManager defaultEventManager;

    @Before
    public void setUp() throws Exception {
        defaultEventManager = new DefaultEventManager(internalDependency);

        when(internalDependency.getName()).thenReturn("my name");
    }

    @Test
    public void eventIsReturned() throws Exception {
        Event event = defaultEventManager.getEvent();

        assertThat(event.getName(), is("Static name here"));
    }
}