package com.jashmore.spikes.multimodule.core;

import org.springframework.stereotype.Service;

@Service
public class InternalDependency {

    public String getName() {
        return "name";
    }
}
