package com.jashmore.spikes.multimodule.core;

import com.jashmore.multimodule.api.Event;
import com.jashmore.multimodule.api.EventManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DefaultEventManager implements EventManager {
    private final InternalDependency internalDependency;

    @Autowired
    public DefaultEventManager(InternalDependency internalDependency) {
        this.internalDependency = internalDependency;
    }

    @Override
    public Event getEvent() {
        System.out.println("Using internal dependency: " + internalDependency.getName());
        return new EventImpl();
    }
}
