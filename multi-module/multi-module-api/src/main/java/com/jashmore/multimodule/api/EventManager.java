package com.jashmore.multimodule.api;

public interface EventManager {
    Event getEvent();
}
