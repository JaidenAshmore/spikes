package com.jashmore.multimodule.api;


public interface Event {
    String getName();
}
