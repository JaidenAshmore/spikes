package com.jashmore.spikes.multimodule.web;


import com.jashmore.multimodule.api.EventManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path="/rest")
public class MyRestController {

    private final EventManager eventManager;

    @Autowired
    public MyRestController(final EventManager eventManager) {
        this.eventManager = eventManager;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String get() {
        return eventManager.getEvent().getName();
    }
}
