package com.jashmore.spikes.multimodule.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.jashmore.spikes.multimodule")
public class MultiModuleWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(MultiModuleWebApplication.class, args);
	}
}
