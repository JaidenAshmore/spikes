## Overview
A spike of how a project split up into multiple modules could work, unfortunately it has a dependency of spring between
them. For example, to dependency inject classes from core into a spring web app you need to annotate the core classes
with spring annotations. It would be nicer if it didn't have this dependency. Something I can look at improving later.