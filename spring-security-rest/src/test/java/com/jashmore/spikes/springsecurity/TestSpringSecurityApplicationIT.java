package com.jashmore.spikes.springsecurity;

import com.jashmore.spikes.springsecurity.util.WebTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.logout;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class TestSpringSecurityApplicationIT extends WebTest {
	@Autowired
	private MockMvc mockMvc;

	@Override
	@Before
	public void setUp() throws Exception {
		final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		final String encodedPassword = encoder.encode("password");
		databaseContainer.runSqlQuery(entityManager -> {
			entityManager.createNativeQuery("insert into users (id, username, password, enabled) values (1, 'user', '" + encodedPassword + "', 'true')")
				.executeUpdate();

			entityManager.createNativeQuery("insert into user_roles (id, username, role) values (1, 'user', 'USER')")
					.executeUpdate();
		});
	}

	@Test
	public void nonExistentUrlsReturnNotFound() throws Exception {
		this.mockMvc.perform(get("/index"))
				.andExpect(status().isNotFound());
	}

	@Test
	public void canLoginToSystem() throws Exception {
		this.mockMvc.perform(formLogin().user("user").password("password"))
				.andExpect(authenticated());
	}

	@Test
	public void unauthenticatedUserCannotViewAuthenticatedPages() throws Exception {
		this.mockMvc.perform(get("/api"))
				.andExpect(status().isUnauthorized());
	}

	@Test
	public void afterLoginUserCanAccessAuthenticatedPages() throws Exception {
		final MvcResult mvcResult = this.mockMvc.perform(formLogin().user("user").password("password"))
				.andExpect(authenticated()).andReturn();

		final MockHttpSession httpSession = (MockHttpSession) mvcResult.getRequest().getSession(false);

		this.mockMvc.perform(get("/api").session(httpSession))
				.andExpect(status().isOk())
				.andExpect(content().string("Hello world"));
	}

	@Test
	public void loggingOutRemovesSession() throws Exception {
		this.mockMvc.perform(formLogin().user("user").password("password"))
				.andExpect(authenticated());

		final MvcResult mvcResult = this.mockMvc.perform(logout())
				.andExpect(status().isOk()).andReturn();

		final MockHttpSession httpSession = (MockHttpSession) mvcResult.getRequest().getSession(false);
		assertThat(httpSession, nullValue());
	}

	@Test
	public void incorrectCredentialsWillNotLoginUser() throws Exception {
		this.mockMvc.perform(formLogin().user("userincorrect").password("password"))
				.andExpect(status().isUnauthorized());
	}

	@Test
	public void loggingOutWhenNotLoggedInDoesNothing() throws Exception {
		this.mockMvc.perform(logout())
				.andExpect(status().isOk());
	}
}
