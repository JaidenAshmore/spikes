package com.jashmore.spikes.springsecurity;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path="/api")
public class ApiController {

    @RequestMapping(method = RequestMethod.GET)
    public String getSomething() {
        return "Hello world";
    }
}
