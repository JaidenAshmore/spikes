package com.jashmore.spikes.springsecurity;

import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

/**
 * Copied from jpa-entities so these are not dependant on each other.
 */
@Configuration
public class PostgresDatabaseConfiguration {
    private static final String PACKAGES_TO_SCAN_FOR_ENTITIES = "com.jashmore.spikes.springsecurity";

    // Environment variables for including the database details in the execution
    private static final String ENV_DB_HOST = "DB_HOST";
    private static final String ENV_DB_PORT = "DB_PORT";
    private static final String ENV_DB_NAME = "DB_NAME";
    private static final String ENV_DB_USERNAME = "DB_USERNAME";
    private static final String ENV_DB_PASSWORD = "DB_PASSWORD";

    // Default db details, this would only be used for local development
    private static final String DEFAULT_DB_HOST = "localhost";
    private static final String DEFAULT_DB_PORT = "5432";
    private static final String DEFAULT_DB_NAME = "example";
    private static final String DEFAULT_DB_USERNAME = "postgres";
    private static final String DEFAULT_DB_PASSWORD = "postgres";

    private final Environment environment;

    @Autowired
    public PostgresDatabaseConfiguration(Environment environment) {
        this.environment = environment;
    }

    @Bean
    public JpaProperties jpaProperties() {
        final JpaProperties jpaProperties = new JpaProperties();
        jpaProperties.setProperties(ImmutableMap.of(
                "hibernate.hbm2ddl.auto", "create-drop"
        ));
        return jpaProperties;
    }

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource driver = new DriverManagerDataSource();
        driver.setDriverClassName("org.postgresql.Driver");
        driver.setUrl(getJdbcUrl());
        driver.setUsername(environment.getProperty(ENV_DB_USERNAME, DEFAULT_DB_USERNAME));
        driver.setPassword(environment.getProperty(ENV_DB_PASSWORD, DEFAULT_DB_PASSWORD));
        return driver;
    }

    @Bean
    public JpaVendorAdapter jpaVendorAdapter() {
        HibernateJpaVendorAdapter bean = new HibernateJpaVendorAdapter();
        bean.setDatabase(Database.POSTGRESQL);
        bean.setShowSql(true);
        bean.setGenerateDdl(true);
        return bean;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            DataSource dataSource, JpaVendorAdapter jpaVendorAdapter) {
        final LocalContainerEntityManagerFactoryBean bean = new LocalContainerEntityManagerFactoryBean();
        bean.setDataSource(dataSource);
        bean.setJpaVendorAdapter(jpaVendorAdapter);
        bean.setPackagesToScan(PACKAGES_TO_SCAN_FOR_ENTITIES);
        return bean;
    }

    @Bean
    public JpaTransactionManager transactionManager(EntityManagerFactory emf) {
        return new JpaTransactionManager(emf);
    }

    public EntityManagerFactory getEntityManagerFactory() {
        JpaProperties jpaProperties = jpaProperties();
        DataSource dataSource = dataSource();
        JpaVendorAdapter jpaVendorAdapter = jpaVendorAdapter();
        LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean = entityManagerFactory(dataSource, jpaVendorAdapter);
        localContainerEntityManagerFactoryBean.setJpaPropertyMap(jpaProperties.getProperties());
        localContainerEntityManagerFactoryBean.afterPropertiesSet();
        return localContainerEntityManagerFactoryBean.getObject();
    }

    private String getJdbcUrl() {
        return String.format("jdbc:postgresql://%s:%s/%s",
                environment.getProperty(ENV_DB_HOST, DEFAULT_DB_HOST),
                environment.getProperty(ENV_DB_PORT, DEFAULT_DB_PORT),
                environment.getProperty(ENV_DB_NAME, DEFAULT_DB_NAME)
        );
    }
}

