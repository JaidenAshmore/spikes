package com.jashmore.spikes.springwebapp.rest;

import com.jashmore.spikes.springwebapp.Visit;
import com.jashmore.spikes.springwebapp.VisitsManager;
import com.jashmore.spikes.springwebapp.util.DatabaseContainer;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

public class TestVisitsManagerIT {

    @Rule
    public DatabaseContainer databaseContainer = new DatabaseContainer();

    private VisitsManager visitsManager;

    @Before
    public void setUp() throws Exception {
        visitsManager = new VisitsManager(databaseContainer.getEntityManagerFactory());
    }

    @Test
    public void visitingSavesIntoDatabase() {
        visitsManager.visit();

        List<Visit> visits = visitsManager.getAll();

        assertThat(visits, hasSize(1));
    }

    @Test
    public void visitingMultipleTimesSavesIntoDatabase() {
        visitsManager.visit();
        visitsManager.visit();
        visitsManager.visit();

        List<Visit> visits = visitsManager.getAll();

        assertThat(visits, hasSize(3));
    }
}
