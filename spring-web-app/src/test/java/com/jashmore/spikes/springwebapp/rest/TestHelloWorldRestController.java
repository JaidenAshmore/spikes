package com.jashmore.spikes.springwebapp.rest;

import com.jashmore.spikes.springwebapp.HelloWorldRestController;
import com.jashmore.spikes.springwebapp.VisitsManager;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class TestHelloWorldRestController {

	private HelloWorldRestController helloWorldRestController;

	@Rule
	public MockitoRule mockitoRule = MockitoJUnit.rule();

	@Mock
	private VisitsManager visitsManager;

	@Before
	public void setUp() throws Exception {
		helloWorldRestController = new HelloWorldRestController(visitsManager);
	}

	@Test
	public void whenVisitingTheVisitsManagerGetsCalled() {
		final String result = helloWorldRestController.visit();

		assertThat(result, is("Welcome back!"));
		verify(visitsManager, times(1)).visit();
	}
}
