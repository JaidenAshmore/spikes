package com.jashmore.spikes.springwebapp.util;

import org.junit.Before;
import org.junit.Rule;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Represents an Integration test that relies on the web application being started.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class WebTest {

    /**
     * Rule used to wipe the database before each test.
     */
    @Rule
    public DatabaseContainer databaseContainer = new DatabaseContainer();

    @LocalServerPort
    private int port;

    private String baseUrl;

    @Before
    public void setUp() throws Exception {
        this.baseUrl = "http://localhost:" + port + "/";
    }

    protected String getBaseUrl() {
        return baseUrl;
    }

}
