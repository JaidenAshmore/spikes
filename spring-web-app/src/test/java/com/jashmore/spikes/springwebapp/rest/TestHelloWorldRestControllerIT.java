package com.jashmore.spikes.springwebapp.rest;

import com.jashmore.spikes.springwebapp.Visit;
import com.jashmore.spikes.springwebapp.util.WebTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

@RunWith(SpringRunner.class)
public class TestHelloWorldRestControllerIT extends WebTest {

    @Autowired
    private TestRestTemplate template;

    @Test
    public void visitRestWelcomesUs() throws Exception {
        final ResponseEntity<String> result = template.getForEntity(getBaseUrl() + "/rest", String.class);

        assertThat(result.getStatusCodeValue(), is(200));
        assertThat(result.getBody(), is("Welcome back!"));
    }

    @Test
    public void visitRestRecordsTimeVisit() {
        template.getForEntity(getBaseUrl() + "/rest", String.class);

        ResponseEntity<List<Visit>> visitors = template.exchange(getBaseUrl() + "/rest/all", HttpMethod.GET, null, new ParameterizedTypeReference<List<Visit>>() {
        });

        assertThat(visitors.getStatusCodeValue(), is(200));
        assertThat(visitors.getBody(), hasSize(1));
    }
}
