package com.jashmore.spikes.springwebapp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

/**
 * Just a simple database entry that logs the time someone hit the REST endpoint. This will be
 * used to show that we can write integration tests that connect to a database.
 */
@Entity
@Table(name="Visit")
public class Visit {

    @Id
    @Column(name = "account_id", nullable = false)
    @GeneratedValue
    private Long id;

    @Column(name = "time")
    private java.sql.Timestamp dateTimeField;

    public Visit() {
    }

    public Visit(Timestamp dateTimeField) {
        this.dateTimeField = dateTimeField;
    }

    public Long getId() {
        return id;
    }

    public Timestamp getDateTimeField() {
        return dateTimeField;
    }
}
