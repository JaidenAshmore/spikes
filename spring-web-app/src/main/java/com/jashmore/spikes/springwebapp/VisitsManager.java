package com.jashmore.spikes.springwebapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;


@Service
public class VisitsManager {
    private final EntityManagerFactory entityManagerFactory;

    @Autowired
    public VisitsManager(final EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    public List<Visit> getAll() {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        return entityManager.createQuery("SELECT v FROM Visit v", Visit.class).getResultList();
    }

    public void visit() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        Visit visit = new Visit(Timestamp.from(Instant.now()));
        entityManager.persist(visit);
        entityManager.getTransaction().commit();
    }
}
