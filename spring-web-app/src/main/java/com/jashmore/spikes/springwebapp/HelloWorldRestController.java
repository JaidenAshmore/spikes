package com.jashmore.spikes.springwebapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/rest")
public class HelloWorldRestController {

    private final VisitsManager visitsManager;

    @Autowired
    public HelloWorldRestController(final VisitsManager visitsManager) {
        this.visitsManager = visitsManager;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String visit() {
        visitsManager.visit();
        return "Welcome back!";
    }

    @RequestMapping(method = RequestMethod.GET, path = "/all")
    public List<Visit> getAll() {
        return visitsManager.getAll();
    }
}
