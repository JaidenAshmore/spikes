package com.jashmore.spikes.jpa.util;

import com.jashmore.spikes.jpa.PostgresDatabaseConfiguration;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.springframework.core.env.Environment;
import org.springframework.core.env.StandardEnvironment;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.function.Consumer;

public class DatabaseContainer implements TestRule {

    private final EntityManagerFactory entityManagerFactory;

    public DatabaseContainer() {
        Environment environment = new StandardEnvironment();
        PostgresDatabaseConfiguration db = new PostgresDatabaseConfiguration(environment);

        entityManagerFactory = db.getEntityManagerFactory();
    }

    public void runSqlQuery(Consumer<EntityManager> entityManagerConsumer) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try {
            entityManagerConsumer.accept(entityManager);
        } finally {
            entityManager.getTransaction().commit();
        }
    }

    @Override
    public Statement apply(Statement base, Description description) {
        return new UsingDatabaseStatement(base);
    }

    public EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }

    private class UsingDatabaseStatement extends Statement {
        private final Statement originalStatement;

        UsingDatabaseStatement(final Statement originalStatement) {
            this.originalStatement = originalStatement;
        }

        @Override
        public void evaluate() throws Throwable {
            wipeDatabase();
            originalStatement.evaluate();
        }

        private void wipeDatabase() {
            runSqlQuery(entityManager -> {
                entityManager.createNativeQuery(
                        "do\n" +
                                "$$\n" +
                                "declare\n" +
                                "  l_stmt text;\n" +
                                "begin\n" +
                                "  select 'truncate ' || string_agg(format('%I.%I', schemaname, tablename), ',')\n" +
                                "    into l_stmt\n" +
                                "  from pg_tables\n" +
                                "  where schemaname in ('public');\n" +
                                "\n" +
                                "  execute l_stmt;\n" +
                                "end;\n" +
                                "$$"
                ).executeUpdate();
            });
        }

    }
}
