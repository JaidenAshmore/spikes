package com.jashmore.spikes.jpa;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableAutoConfiguration
public class JpaEntitiesApplication {

	public static void main(String[] args) {
		SpringApplication.run(JpaEntitiesApplication.class, args);
	}

	/**
	 * Sets up some test users for manual testing.
	 */
	@Bean
	CommandLineRunner init(AccountManager accountManager) {
		return (evt) -> {
			accountManager.deleteAllAccounts();
			Account account = new Account("bob");
			accountManager.saveAccount(account);
			System.out.println("Constructed, let's go" + accountManager.getAllAccounts().toString());
		};
	}
}
