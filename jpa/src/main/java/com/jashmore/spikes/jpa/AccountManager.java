package com.jashmore.spikes.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.Collection;

@Service
public class AccountManager {

    private final EntityManagerFactory entityManagerFactory;

    @Autowired
    public AccountManager(final EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    public Collection<Account> getAllAccounts() {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        return entityManager.createQuery("SELECT a FROM Account a", Account.class).getResultList();
    }

    public void deleteAllAccounts() {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.createQuery("DELETE FROM Account").executeUpdate();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public void saveAccount(final Account account) {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(account);
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
