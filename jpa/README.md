## Overview
This module gives a very simple example of using JPA Persistence to work with a database.

## Implementation
- Account: The account class represents the table in the system. It defines the table names and
  the columns.
- AccountManager: The manager for accessing and manipulating account entities.
- PostgresDatabaseConfiguration: This sets up the database connection, ultimately creating an
  EntityManagerFactory to be used for persistence.
- JpaEntitiesApplication: A simple Spring application that uses the account manager and connects
  to the database.

## Testing
- All of the tests that have an IT suffix are run in the integration test phase.
- The other tests are all unit tests and do not have a database set up to use.
- When the integration test is called a docker image is created with a postgres database for the
  integration test to use. It uses environment variables to set up the settings for this
- When running locally you can use environment variables to point to a local database or just
  use the default values (though you need a database set up for that).


## Usage
- mvn test: runs unit tests
- mvn verify: runs integration tests, a docker image with postgres is created before and destroyed
  after the tests
- mvn spring-boot:run: runs the application. This implementation is not a web app