package com.jashmore.spikes.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Account")
public class Account {
    @Id
    @Column(name = "account_id", nullable = false)
    @GeneratedValue
    private Long id;

    @Column(name = "user_name", nullable = false)
    private String username;

    //Used for JPA only
    public Account() {
    }

    public Account(final String username) {
        this.username = username;
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }
}
