package com.jashmore.spikes.jpa;

import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static com.jashmore.spikes.jpa.QAccount.account;
import static com.jashmore.spikes.jpa.QBook.book;

@Service
public class AccountManager {

    private final EntityManagerFactory entityManagerFactory;

    @Autowired
    public AccountManager(final EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    public Collection<Account> getAllAccounts() {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);

        return queryFactory.selectFrom(account)
                .fetch();
    }

    public Optional<Account> getAccount(String name) {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);

        return Optional.ofNullable(queryFactory.selectFrom(account)
                .where(account.username.eq(name))
                .fetchOne());

    }

    public List<Book> getAccountBooks(Account account) {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);

        return queryFactory.selectFrom(book)
                .where(book.account.id.eq(account.getId()))
                .fetch();

    }

    public void deleteAllAccounts() {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        queryFactory.delete(account)
                .execute();
    }

    public void saveAccount(final Account account) {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(account);
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
