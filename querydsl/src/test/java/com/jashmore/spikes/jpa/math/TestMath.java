package com.jashmore.spikes.jpa.math;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class TestMath {

    @Test
    public void addingPositiveNumbersResultsInPositive() {
        Math math = new Math();

        int result = math.add(1, 3);

        assertThat(result, equalTo(4));
    }
}