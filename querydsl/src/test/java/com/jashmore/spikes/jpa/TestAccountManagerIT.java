package com.jashmore.spikes.jpa;

import com.google.common.collect.ImmutableList;
import com.jashmore.spikes.jpa.util.DatabaseContainer;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import javax.persistence.EntityManagerFactory;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;

public class TestAccountManagerIT {
    @Rule
    public DatabaseContainer databaseContainer = new DatabaseContainer();

    private EntityManagerFactory entityManagerFactory;

    private AccountManager accountManager;

    @Before
    public void setUp() throws Exception {
        entityManagerFactory = databaseContainer.getEntityManagerFactory();
        accountManager = new AccountManager(entityManagerFactory);
    }

    @Test
    public void canReadTheAccountFromTheDatabase() {
        databaseContainer.runSqlQuery(entityManager -> {
            entityManager.createNativeQuery(
                    "insert into Account (account_id, user_name) values (1, 'bob')"
            ).executeUpdate();
        });

        final List<Account> accounts = ImmutableList.copyOf(accountManager.getAllAccounts());
        assertThat(accounts, hasSize(1));
        assertThat(accounts.get(0).getUsername(), equalTo("bob"));
    }

    @Test
    public void savingAnAccountPersistsIt() {
        accountManager.saveAccount(new Account("bob"));
        Collection<Account> accounts = accountManager.getAllAccounts();
        assertThat(accounts, hasSize(1));
    }

    @Test
    public void canGetAllBooksForAnAccount() {
        accountManager.saveAccount(new Account("bob"));
        accountManager.saveAccount(new Account("jane"));

        Account bobAccount = accountManager.getAccount("bob").orElseThrow(() -> new IllegalStateException("Error"));
        Account janeAccount = accountManager.getAccount("jane").orElseThrow(() -> new IllegalStateException("Error"));
        databaseContainer.runSqlQuery(entityManager -> {
            final Book book1 = new Book(bobAccount, "book1");
            entityManager.persist(book1);
            final Book book2 = new Book(bobAccount, "book2");
            entityManager.persist(book2);
            entityManager.persist(new Book(janeAccount, "book 3"));
        });

        assertThat(accountManager.getAccountBooks(bobAccount), hasSize(2));
        assertThat(accountManager.getAccountBooks(janeAccount), hasSize(1));
    }
}